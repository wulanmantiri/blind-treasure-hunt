# Blind Treasure Hunt [POK]

## Tentang Aplikasi Ini
Permainan ini mensimulasikan pencarian harta karun dimana sang pencari tidak dapat melihat apa-apa. Selama menelusuri *maze*, pencari tidak dapat melihat apa-apa. Akan tetapi, pencari dapat mengetahui jika ia menjadi lebih dekat atau lebih jauh ke harta karun yang dicari. Pencari juga dapat mengetahui jika ia terhalang oleh tembok pada *maze* tersebut.
**Jika pencari dapat menemukan harta karun sebelum waktu habis, maka pemain akan menang. Sebaliknya, jika tidak, pemain akan dinyatakan kalah**.

## Pembuat [Kelompok 5 Kelas POK C - *anyway*]:
* **Andrew Theodore** - 1806133862
* **Nunun Hidayah** - 1806141403
* **Wulan Mantiri** - 1806205666

## Petunjuk Penggunaan Aplikasi:
1. Pertama, *build* program tersebut.
2. Menjalankan program.
3. Akan muncul beberapa tulisan di LCD yang menampilkan judul permainan dan cara bermain.
4. Setelah tulisan terakhir muncul, yaitu `Can you? Click Start!`, menekan tombol start untuk memulai permainan.
5. Kemudian, kita menekan keypad untuk menggerakan karakter dan akan muncul respon warna di LED.
6. Apabila pemain sudah berada di harta karun, maka akan muncul tulisan *Congratulations*.
7. Jika pemain belum menemukan harta karun pada waktu yang diberikan, maka akan ada tulisan bahwa anda sudah kalah.

## Acknowledgements
* POK CSUI 2019
* Teaching Assistant : Aji Bramantio

> Projek ini merupakan pemenuhan tugas akhir POK CSUI 2019