;====================================================================
; BLIND TREASURE HUNT
; Processor     : ATmega8515
; Compiler      : AVRASM
; Authors       : Kelompok 5 POK C - anyway
; Members       : Wulan Mantiri
;               : Nunun Hidayah
;               : Andrew Theodore
;====================================================================

;====================================================================
; DEFINITIONS
;====================================================================
.include "m8515def.inc"

; temporary registers
.def temp = r16 
.def tmp = r17

; keypad and array indexing
.def pos = r20
.def key = r21
.def index = r24
.def A = r25

; led and lcd related
.def led_data = r22
.def PB = r23

.equ WALL = 0x23
.equ FINISH = 0x46
.equ FINISH_LINE = 0x0A
.equ BORDER_TOP_LEFT = 0x00
.equ BORDER_TOP_RIGHT = 0x0F
.equ BORDER_BOTTOM_LEFT = 0x10
.equ BORDER_BOTTOM_RIGHT = 0x1F

;====================================================================
; RESET and INTERRUPT VECTORS
;====================================================================
.org $00
rjmp MAIN
.org $01
rjmp ext_int0
.org $05
rjmp ISR_TCOM1

;====================================================================
; CODE SEGMENT
;====================================================================

MAIN:

INIT_STACK:
    ldi temp, low(RAMEND)
    out SPL, temp
    ldi temp, high(RAMEND)
    out SPH, temp

rcall INIT_LCD_MAIN

INIT_LED:
    ser temp
    out DDRC,temp
    rcall RESET_LED

INIT_INTERRUPT:
    ldi temp, 0b00000010
    out MCUCR, temp
    ldi temp, 0b01000000
    out GICR, temp
    sei

rcall INIT_DEF
forever:
	cpi ZL, 0x00
	brne forever

rcall INIT_TIMER
ldi tmp, 0	; Compare if game over
sei
rjmp INIT_ARRAY

INIT_DEF:
	rcall CLEAR_LCD
	ldi ZH,high(2*titlemsg) 
	ldi ZL,low(2*titlemsg)    

DEFAULT_LCD:
    lpm ; Load byte from program memory into r0
        
    mov A,r0
    tst r0 ; Check if we've reached the end of the message
    breq END_DEFAULT ; If so, quit

    mov A, r0 ; Put the character onto Port B
    rcall DEFAULT_WRITE
    adiw ZL,1 ; Increase Z registers
    ldi  temp, 0
    out  TCNT1L, temp
    ldi  temp, 0
    out  TCNT1H, temp
    rjmp DEFAULT_LCD

DEFAULT_WRITE:
    sbi PORTA,1 ; SETB RS
    out PORTB, A
    sbi PORTA,0 ; SETB EN
    cbi PORTA,0 ; CLR EN
    ret

END_DEFAULT:
	ret

ext_int0:
    cbi PORTA,1 ; CLR RS
    ldi PB,$C0 ; MOV DATA,0x01
    out PORTB,PB
    sbi PORTA,0 ; SETB EN
    cbi PORTA,0 ; CLR EN
    ldi ZH,high(2*startmsg) 
	ldi ZL,low(2*startmsg)    
	rcall DEFAULT_LCD

	ldi ZH, HIGH(2*gamearr)
    ldi ZL, LOW(2*gamearr)
	reti

;====================================================================
; START GAME
;====================================================================
INIT_ARRAY:
    ldi ZH, HIGH(2*gamearr)
    ldi ZL, LOW(2*gamearr)
    lpm
    adiw ZL, 17     
    ldi index, 9            ; distance = 11 - 2 steps       ;
    rjmp PRESS_KEY

check_up:
    ori key,0b00001111
    cpi key,0b11011111
    breq GO_UP
    brne PRESS_KEY
    
check_dir:
    ori key,0b00001111
    cpi key,0b11101111
    breq GO_LEFT
    cpi key, 0b11011111
    breq GO_DOWN
    cpi key, 0b10111111
    breq GO_RIGHT
    brne PRESS_KEY

RESTART_GAME:
	rjmp MAIN

PRESS_KEY:
    ldi temp,0b00001111 ;HIGH - input, LOW - output
    out DDRC,temp 
    ldi temp,0b11110000 ;initialize 0 to output (LOW)
    out PORTC,temp
	cpi tmp, 1
	breq RESTART_GAME

LISTEN_KEY:
    in temp,PINC        ;Read Key HIGH
    cpi temp,0b11110000 ;if all one, no key pressed
    breq LISTEN_KEY

READ_KEY:
    mov key, temp       ;To Check HIGH later
    ldi temp,0b11110000 ;HIGH - output, LOW - input
    out DDRC,temp    
    ldi temp,0b00001111 ;initialize 0 to output (HIGH)
    out PORTC,temp
     
    in temp, PINC       ;Read Key LOW
    ori temp,0b11110000
    cpi temp,0b11111110
    breq check_up    
    cpi temp,0b11111101
    breq check_dir
    brne PRESS_KEY

GO_UP:
    cpi ZL, BORDER_BOTTOM_LEFT
    brlt PRESS_KEY
    subi ZL, 16

    lpm
    mov A, r0
    rcall check_finish
    cpi A, WALL
    brne warmer
    rcall hit_wall
    adiw ZL, 16
    rjmp PRESS_KEY

GO_LEFT:
    cpi ZL, BORDER_TOP_LEFT
    breq PRESS_KEY
    cpi ZL, BORDER_BOTTOM_LEFT
    breq PRESS_KEY
    mov pos, ZL         ; old position
    subi ZL, 1

    lpm
    mov A, r0
    rcall check_finish
    cpi A, WALL
    breq UNDO_LEFT
    cpi ZL, BORDER_BOTTOM_LEFT
    brlt compare_pos_left
    subi pos, 16
    rjmp compare_pos_left

GO_DOWN:
    cpi ZL, BORDER_BOTTOM_LEFT
    brge PRESS_KEY
    adiw ZL, 16

    lpm
    mov A, r0
    rcall check_finish
    cpi A, WALL
    brne colder
    rcall hit_wall
    subi ZL, 16
    rjmp PRESS_KEY

GO_RIGHT:
    cpi ZL, BORDER_TOP_RIGHT
    breq PRESS_KEY
    cpi ZL, BORDER_BOTTOM_RIGHT
    breq PRESS_KEY
    mov pos, ZL
    adiw ZL, 1

    lpm
    mov A, r0
    rcall check_finish
    cpi A, WALL
    breq UNDO_RIGHT
    cpi ZL, 0xD0
    brlt compare_pos_right
    subi pos, 16
    rjmp compare_pos_right

UNDO_LEFT:
    rcall hit_wall
    adiw ZL, 1
    rjmp PRESS_KEY

UNDO_RIGHT:
    rcall hit_wall
    subi ZL, 1
    rjmp PRESS_KEY

warmer:
    ldi led_data,0b00000011
    out PORTE,led_data
    rcall DELAY_02
    rcall RESET_LED
    rjmp PRESS_KEY

colder:
    ldi led_data,0b00001100
    out PORTE,led_data ; 
    rcall DELAY_02
    rcall RESET_LED
    rjmp PRESS_KEY

compare_pos_left:
    subi pos, FINISH_LINE
    cpi pos, 0
    brge sub_index

add_index:
    adiw index,1
    rjmp colder

compare_pos_right:
    subi pos, FINISH_LINE
    cpi pos, 0
    brge add_index

sub_index:
    subi index, 1
    rjmp warmer 

RESET_LED:
    ldi led_data,0x00
    out PORTE,led_data ; Update LEDS
    ret

hit_wall:
    ldi led_data,0b00110000
    out PORTE,led_data
    rcall DELAY_02
    rcall RESET_LED
    ret

check_finish:
    cpi A, FINISH
    breq end_game
    ret

end_game:
    ldi led_data,0b11000000
    out PORTE,led_data
    rcall DELAY_02
    rcall you_won
	rjmp MAIN

INIT_TIMER:
    ldi tmp, (1<<CS12) | (1<<CS10); 
    out TCCR1B,tmp          
    ldi tmp,1<<OCF1B
    out TIFR,tmp       ; Interrupt if compare true in T/C0
    ldi tmp,1<<OCIE1B
    out TIMSK,tmp      ; Enable Timer/Counter0 compare int
    ldi tmp, 0b00110101
    out OCR1BH, tmp
    ldi tmp, 0b00000000
    out OCR1BL, tmp
	sei
    ret

ISR_TCOM1:
    rcall game_over
	ldi tmp, 1
    reti

;====================================================================
; LCD
;====================================================================
INPUT_TEXT:
    ldi ZH,high(2*message) 
    ldi ZL,low(2*message)
    ret

INIT_LCD_MAIN:
    rcall INIT_LCD
    ser temp
    out DDRA,temp ; Set port A as output
    out DDRB,temp ; Set port B as output
    rcall INPUT_TEXT

LOADBYTE:
    lpm 
        
    mov A,r0
    cpi A,1
    breq NEWLINE_LCD
    cpi A,2
    breq NEW_LOAD

    tst r0 ; Check if we've reached the end of the message
    breq END_LCD ; If so, quit

    mov A, r0 ; Put the character onto Port B
    rcall WRITE_TEXT
    adiw ZL,1
    ldi  tmp, 0
    out  TCNT1L, tmp
    ldi  tmp, 0
    out  TCNT1H, tmp
    rjmp LOADBYTE

NEW_LOAD:
    rcall CLEAR_LCD
    adiw ZL,1
    rjmp LOADBYTE

END_LCD:
    rcall CLEAR_LCD
    ret

INIT_LCD:
    cbi PORTA,1 ; CLR RS
    ldi PB,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
    out PORTB,PB
    sbi PORTA,0 ; SETB EN
    cbi PORTA,0 ; CLR EN
    rcall DELAY_01
    cbi PORTA,1 ; CLR RS
    ldi PB,$0E ; MOV DATA,0x0E --> disp ON, cursor ON, blink OFF
    out PORTB,PB
    sbi PORTA,0 ; SETB EN
    cbi PORTA,0 ; CLR EN
    rcall DELAY_01
    rcall CLEAR_LCD ; CLEAR LCD
    cbi PORTA,1 ; CLR RS
    ldi PB,$06 ; M OV DATA,0x06 --> increase cursor, display sroll OFF
    out PORTB,PB
    sbi PORTA,0 ; SETB EN
    cbi PORTA,0 ; CLR EN
    rcall DELAY_01
    ret

CLEAR_LCD:
    rcall DELAY_02
    cbi PORTA,1 ; CLR RS
    ldi PB,$01 ; MOV DATA,0x01
    out PORTB,PB
    sbi PORTA,0 ; SETB EN
    cbi PORTA,0 ; CLR EN
    rcall DELAY_01
    ret

NEWLINE_LCD:
    cbi PORTA,1 ; CLR RS
    ldi PB,$C0 ; MOV DATA,0x01
    out PORTB,PB
    sbi PORTA,0 ; SETB EN
    cbi PORTA,0 ; CLR EN
    rcall DELAY_01
    adiw ZL,1 ; Increase Z registers
    rjmp LOADBYTE

WRITE_TEXT:
    sbi PORTA,1 ; SETB RS
    out PORTB, A
    sbi PORTA,0 ; SETB EN
    cbi PORTA,0 ; CLR EN
    rcall DELAY_01
    ret

game_over:
	rcall CLEAR_LCD
    ldi ZH,high(2*gameover) 
    ldi ZL,low(2*gameover)
    rcall LOADBYTE
    ret

you_won:
	rcall CLEAR_LCD
    ldi ZH,high(2*youwon) 
    ldi ZL,low(2*youwon)
    rcall LOADBYTE
    ret

DELAY_01:
	    ldi  r18, 40
        ldi  r19, 200
    L1: dec  r19
        brne L2
        dec  r18
        brne L2
        nop
        ret

DELAY_02:
; Generated by delay loop calculator
; at http://www.bretmulvey.com/avrdelay.html
;
; Delay 160 000 cycles
; 20ms at 8.0 MHz
        ldi  r18, 208
        ldi  r19, 202
    L2: ldi  tmp, 0
        out  TCNT1H, tmp
        ldi  tmp, 0
        out  TCNT1L, tmp
        
    	dec  r19
        brne L2
        dec  r18
        brne L2
        nop
        ret

;====================================================================
; GAME ARRAY 2 x 16
; # (0x23) REPRESENTS WALL
; S (0x53) REPRESENTS START at index 2 (0x11)
; F (0x46) REPRESENTS FINISH at index 11 (0x0A)
;====================================================================
.org $E00
gamearr:
.db "    #    #F      S    #      #  ", 0

message:
.db "Blind Treasure Hunt",2
.db "Use the keypads to move",1,"No map will be shown",2,2
.db "Only 1 minute to find it!",1,"Can you? Click Start!",0

titlemsg:
.db "Blind Treasure Hunt",0

startmsg:
.db "60 seconds starts NOW!", 0

gameover:
.db "Time's Up! You Lost.",1,"GAME OVER", 0

youwon:
.db "Congratulations! You Won!",1, "Can you beat me again?", 0

gamepseudo:
.db "    #    #F     ", 0 
.db " S    #      #  ", 0
